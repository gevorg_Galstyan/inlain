/**
 * Created by gevor on 18.08.2020.
 */
function comment(e) {
    e.preventDefault();
    let modal = document.getElementsByClassName('comment-modal')[0]
    if (!modal.classList.contains('active')) {
        modal.classList.add('active');

    } else {
        modal.classList.remove('active');
    }
}

function stars(e, self) {
    let nodes = Array.prototype.slice.call(document.getElementsByClassName('comment-modal__body_user_stars')[0].children);
    let len = document.getElementsByClassName('comment-modal__body_user_stars-label').length;
    for (let i = 0; i <= len; i++) {
        document.getElementsByClassName('comment-modal__body_user_stars-label-img')[i].setAttribute('src', "assets/img/stars/star-disable.svg");
        if (i <= nodes.indexOf(self)) {
            document.getElementsByClassName('comment-modal__body_user_stars-label-img')[i].setAttribute('src', "assets/img/stars/star.svg");
        }
    }

}

function closeModal(e) {
    if(e.target.classList.contains('comment-modal')){
        document.getElementsByClassName('comment-modal')[0].classList.remove('active');
    }
    if(e.target.classList.contains('gallery-modal')){
        document.getElementsByClassName('gallery-modal')[0].classList.remove('active');
    }
}

function openImg(e, self) {
    let thisImg = self.children;
    console.log();
    let modal = document.getElementsByClassName('gallery-modal')[0];
    document.getElementsByClassName('gallery-modal__content_img')[0].setAttribute('src', thisImg[0].getAttribute('src'))
    if (!modal.classList.contains('active')) {
        modal.classList.add('active');
    } else {
        modal.classList.remove('active');
    }

}